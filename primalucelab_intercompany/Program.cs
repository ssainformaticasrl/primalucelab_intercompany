﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
//
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;

namespace primalucelab_intercompany
{
    class Program
    {
        public static SAPbobsCOM.Company oCompany;
        static long lLetti, lAggiunti, lModificati;
        static string sID = "";

        static void Main(string[] args)
        {
            string sError = "";
            string filePath = Path.Combine(Path.GetTempPath(), "Intercompany_Log.txt");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            StreamWriter sw = File.CreateText(filePath);
            //Leggo i parametri di connessione al server SQL  
            SqlCommand command = null;
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string sSQL = "";
            sw.WriteLine("Nuova esecuzione: " + DateTime.Now.ToString());
            sw.WriteLine("");
            //sw.WriteLine("ConnectionString:" + connectionString);
            //sw.WriteLine("");            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                //
                sw.WriteLine("UPDATE ARTICOLI");
                Console.WriteLine("UPDATE ARTICOLI");
                string sVal = EXP_OITM(sw, connection);
                //
                sw.Close();
                connection.Close();
                connection.Dispose();
                GC.Collect();

            }
        }

        private static string EXP_OITM(StreamWriter sw, SqlConnection connection)
        {

            lLetti = 0;
            lAggiunti = 0;
            lModificati = 0;

            //SAP connection
            string sVal = "";
            int lRetCode = CompanyConnect();
            int lErrCode;
            string sErrMsg;
            //...
            if (lRetCode != 0)
            {
                oCompany.GetLastError(out lErrCode, out sErrMsg);
                sErrMsg = "Error: " + sErrMsg + "; Code: " + lErrCode;
                return sErrMsg;
            }

            SAPbobsCOM.Recordset oRecordSet;
            #region LISTINI
            string sListino = "Retail price list";
            int iListino = 0;
            string sSql = "SELECT T0.\"ListNum\" FROM OPLN T0 WHERE T0.\"ListName\" ='" + sListino + "'";
            oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordSet.DoQuery(sSql);
            iListino = Convert.ToInt32(oRecordSet.Fields.Item("ListNum").Value.ToString().Trim());
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;
            GC.Collect();
            if (iListino == 0)
            {
                sw.WriteLine("Errore:" + sListino + " non trovato.");
                Console.WriteLine("Errore:" + sListino + " non trovato.");
                return "";
            }
            #endregion

            string sSQL = @"SELECT T0.[ItemCode], T0.[ItemName], T0.[FrgnName], T0.[ItmsGrpCod], T1.[ItmsGrpNam], T0.[PrchseItem], T0.[SellItem], T0.[InvntItem], T0.[FirmCode], T2.[FirmName], T0.[BuyUnitMsr], T0.[SalUnitMsr], T0.[InvntryUom], T0.[U_EANCode], T_.[Price] FROM OITM T0  LEFT JOIN OITB T1 ON T0.ItmsGrpCod = T1.ItmsGrpCod LEFT JOIN OMRC T2 ON T0.FirmCode = T2.FirmCode 
LEFT JOIN (SELECT T3.[ItemCode], T3.[Price] FROM OPLN T4 
INNER JOIN ITM1 T3 ON T3.[PriceList] = T4.[ListNum] and T4.[ListName]='List.Prezzi BP Dollari') T_ ON T_.ItemCode=T0.[ItemCode]
WHERE T0.[QryGroup6] ='Y' 
ORDER BY T0.[ItemCode]";
            sw.WriteLine("SQL:" + sSQL);
            sw.WriteLine("");
            SqlCommand command = new SqlCommand(sSQL, connection);
            SqlDataReader reader = command.ExecuteReader();
            long lngVal = 0;
            SAPbobsCOM.Items oItem;
            SAPbobsCOM.Items_Prices oItems_Prices;
            bool b_TestExist;
            int iVal;
            double dVal;
            List<string> listLING = new List<string>();
            try
            {

                while (reader.Read())
                {

                    sID = reader["ItemCode"].ToString().Trim();
                    lngVal = (int)lLetti % 100;
                    if (lngVal == 0)
                    {
                        sVal = "Letti " + lLetti.ToString().Trim() + " aggiunti " + lAggiunti.ToString().Trim();
                        sw.WriteLine(sVal);
                        Console.WriteLine(sVal);
                    }

                    //
                    oCompany.StartTransaction();
                    oItem = (SAPbobsCOM.Items)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    b_TestExist = oItem.GetByKey(sID);
                    if (!(b_TestExist))
                    {
                        //Se si tratta di un nuovo record ...                        
                        //... Codice Articolo
                        oItem.ItemCode = sID;

                        #region TIPO ARTICOLO ...
                        sVal = reader["PrchseItem"].ToString().Trim();
                        if (sVal == "Y")
                        {
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        }
                        else
                        {
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tNO;
                        }
                        sVal = reader["SellItem"].ToString().Trim();
                        if (sVal == "Y")
                        {
                            oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        }
                        else
                        {
                            oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tNO;
                        }
                        sVal = reader["InvntItem"].ToString().Trim();
                        if (sVal == "Y")
                        {
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        }
                        else
                        {
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tNO;
                        }
                        #endregion
                    }
                    oItem.DefaultWarehouse = "01";
                    oItem.ItemName = reader["ItemName"].ToString().Trim();
                    oItem.ForeignName = reader["FrgnName"].ToString().Trim();

                    #region GRUPPO_ARTICOLO
                    sVal = reader["ItmsGrpNam"].ToString().Trim();
                    if (sVal != "")
                    {
                        if (sVal == "Semilavorati")
                        {
                            sVal = "Semi-finished prod.";
                        }
                        if (sVal == "Prodotti Finiti")
                        {
                            sVal = "Finished products";
                        }
                        oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRecordSet.DoQuery("SELECT ItmsGrpCod,ItmsGrpNam FROM OITB WHERE ItmsGrpNam='" + sVal + "' ORDER BY ItmsGrpCod");
                        iVal = Convert.ToInt32(oRecordSet.Fields.Item("ItmsGrpCod").Value);
                        if (iVal != 0)
                        {
                            oItem.ItemsGroupCode = iVal;
                        }
                        else
                        {
                            sw.WriteLine(sID + ": Non è stato trovato il gruppo articoli: " + sVal);
                            Console.WriteLine(sID + ": Non è stato trovato il gruppo articoli: " + sVal);
                            return "";
                        }
                        oRecordSet = null;
                        GC.Collect();
                    }
                    #endregion

                    #region PRODUTTORE
                    sVal = reader["FirmName"].ToString().Trim();
                    if (sVal != "")
                    {
                        if (sVal == "- Nessun produttore -")
                        {
                            sVal = "- No Manufacturer -";
                        }
                        oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRecordSet.DoQuery("SELECT T0.[FirmCode] FROM OMRC T0 WHERE T0.[FirmName] ='" + sVal + "'");
                        iVal = Convert.ToInt32(oRecordSet.Fields.Item("FirmCode").Value);
                        if (iVal != 0)
                        {
                            oItem.Manufacturer = iVal;
                        }
                        else
                        {
                            sw.WriteLine(sID + ": Non è stato trovato il produttore: " + sVal);
                            Console.WriteLine(sID + ": Non è stato trovato il produttore: " + sVal);
                            return "";
                        }
                        oRecordSet = null;
                        GC.Collect();
                    }
                    #endregion

                    #region UNITA DI MISURA                    
                    oItem.PurchaseUnit = reader["BuyUnitMsr"].ToString().Trim();//... d'acquisto
                    oItem.SalesUnit = reader["SalUnitMsr"].ToString().Trim(); //... vendita
                    oItem.InventoryUOM = reader["InvntryUom"].ToString().Trim(); //... magazzino                    
                    #endregion

                    oItem.UserFields.Fields.Item("U_EANCode").Value = reader["U_EANCode"].ToString().Trim();

                    #region LISTINO
                    dVal = 0;
                    sVal = reader["Price"].ToString().Trim();
                    if (sVal != "")
                    {
                        //Numeric
                        dVal = Convert.ToDouble(sVal);
                    }
                    oItems_Prices = oItem.PriceList;
                    for (iVal = 0; iVal < oItems_Prices.Count; iVal++)
                    {
                        oItems_Prices.SetCurrentLine(iVal);
                        if (oItems_Prices.PriceListName == sListino)
                        {
                            oItems_Prices.Price = dVal;
                            //oItems_Prices.Currency = "USD";
                            break;
                        }
                    }
                    #endregion

                    if (b_TestExist)
                    {
                        //se si NON tratta di un nuovo record
                        lRetCode = oItem.Update();
                        lModificati = lModificati + 1;
                    }
                    else
                    {
                        //se si tratta di un nuovo record        
                        lRetCode = oItem.Add();
                        lAggiunti = lAggiunti + 1;
                    }

                    if (lRetCode != 0)
                    {
                        sVal = sID;
                        oCompany.GetLastError(out lErrCode, out sErrMsg);
                        sVal = sID + ": " + sErrMsg + " (" + lErrCode + ")\r\n";
                        sw.WriteLine(sVal);
                        Console.WriteLine(sVal);
                        return "";
                    }

                    if (oCompany.InTransaction)
                    {
                        //oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    }
                    else
                    {
                        throw new Exception("ERROR: Transaction closed before EndTransaction");
                    }

                    lLetti++;
                }

            }
            catch (Exception ex)
            {
                //logFile("Errore:" + ex.Message);
                //logFile("Try Parse Ultima Lettura File: " + sVar);
                sw.WriteLine("Errore:" + ex.Message);
                Console.WriteLine("Errore:" + ex.Message);
                return ex.Message;
            }
            finally
            {
                // Always call Close when done reading.                
                reader.Close();
                CompanyDisconnect();
            }
            return "";
        }


        private static int CompanyConnect()
        {
            oCompany = new SAPbobsCOM.Company();

            oCompany.Server = ConfigurationManager.ConnectionStrings["Server"].ConnectionString;
            oCompany.CompanyDB = ConfigurationManager.ConnectionStrings["CompanyDB"].ConnectionString;
            oCompany.UserName = ConfigurationManager.ConnectionStrings["UserName"].ConnectionString;
            oCompany.Password = ConfigurationManager.ConnectionStrings["UserPsw"].ConnectionString;
            if (ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString == "MSSQL2014")
            {
                oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
            }
            //oCompany.LicenseServer = "localhost:30000";
            oCompany.LicenseServer = ConfigurationManager.ConnectionStrings["License_Server"].ConnectionString;
            //oCompany.SLDServer= ConfigurationManager.ConnectionStrings["SLD_Server"].ConnectionString;


            return oCompany.Connect();
        }

        private static void CompanyDisconnect()
        {
            if (oCompany != null)
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
                oCompany = null;
            }
            if (oCompany != null)
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCompany);
                oCompany = null;
            }
        }
    }
}
